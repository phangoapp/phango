<?php

use PhangoApp\PhaModels\WPDO;
use PhangoApp\PhaUtils\Utils;
use PHPUnit\Framework\TestCase;

include("vendor/autoload.php");

Utils::load_config('config', 'settings');

$sql_table="CREATE TABLE table_test (
    `name` VARCHAR(255) NOT NULL default '',
    `last_name` VARCHAR(255) NOT NULL default '',
    `type` INT NOT NULL
);";

$pdo=new WPDO('table_test', ['name', 'last_name', 'type']);

$pdo->connect();

final class WPDOTest extends TestCase {

    public function testCreateTable()
	{
		global $pdo;
		
		$this->assertTrue($pdo->query($sql_table));
	
	}
    
    /**
	* @depends testCreateTable
	*/
	
	public function testDropTable()
	{
        global $pdo;
		
		$this->assertTrue($pdo->query('drop table table_test'));
	
	}

}
