<?php

$arr_composer=array(

    "name" => "phangoapp/framework",
    "description" => "A framework for create nice apps",
    "php" => "^5.4|| ^8.2",
     "license" => "GPL",
    "authors"=> [
        array(
            "name"=> "Antonio de la Rosa",
            "email"=> "webmaster@web-t-sys.com"
        )
    ],
    
    "minimum-stability" => "stable",

    "repositories" => [array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/pharouter.git"
       ), 
       array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/phaview.git"
        ),
	array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/phamodels.git"
        ),
        array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/phautils.git"
        ),
	array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/phai18n.git"
        ),
	array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/phalibs.git"
        ),
        array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/admin.git"
        ),
	 array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/welcome.git"
        ),
         array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/lang.git"
        ),
	array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/phatime.git"
        ),
        array(
            "type"=> "git",
            "url"=> "https://bitbucket.org/phangoapp/showmedia.git"
        )
	],

    "require" => array(

	"phangoapp/pharouter"=> "dev-master",
        "phangoapp/phaview"=> "dev-master",
	"phangoapp/phamodels"=> "dev-master",
	"phangoapp/phautils"=> "dev-master",
	"phangoapp/phai18n"=> "dev-master",
	"phangoapp/phalibs"=> "dev-master",
	"phangoapp/admin"=> "dev-master",
	"phangoapp/welcome"=> "dev-master",
	"phangoapp/lang"=> "dev-master",
	"phangoapp/phatime"=>"dev-master",
	"phangoapp/showmedia"=>"dev-master",
	"ext-gd"=> "*",
	"ext-libxml"=> "*",
	"league/climate"=> "@stable"
	
    )

);

?>
